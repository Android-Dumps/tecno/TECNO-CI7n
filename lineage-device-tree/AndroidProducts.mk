#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_TECNO-CI7n.mk

COMMON_LUNCH_CHOICES := \
    lineage_TECNO-CI7n-user \
    lineage_TECNO-CI7n-userdebug \
    lineage_TECNO-CI7n-eng
