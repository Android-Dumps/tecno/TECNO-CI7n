#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from TECNO-CI7n device
$(call inherit-product, device/tecno/TECNO-CI7n/device.mk)

PRODUCT_DEVICE := TECNO-CI7n
PRODUCT_NAME := lineage_TECNO-CI7n
PRODUCT_BRAND := TECNO
PRODUCT_MODEL := TECNO CI7n
PRODUCT_MANUFACTURER := tecno

PRODUCT_GMS_CLIENTID_BASE := android-transsion

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_tssi_64_armv82_tecno-user 12 SP1A.210812.016 294843 release-keys"

BUILD_FINGERPRINT := TECNO/CI7n-GL/TECNO-CI7n:12/SP1A.210812.016/220508V619:user/release-keys
